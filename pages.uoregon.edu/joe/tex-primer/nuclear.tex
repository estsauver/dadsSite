% Save file as: NUCLEAR.TEX            Source: FILESERV@SHSU.BITNET  
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% nuclear.tex: sample tex document
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\input fontsize.tex
\tenpoint
\baselineskip=1.5\normalbaselineskip
\parindent=0pt \global\skip\footins=24pt plus 24pt minus 24pt
\def\footnoterule{\vfil \vglue 12pt \hrule width 2in \vglue 12pt}
\nopagenumbers
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
[The following article originally appeared in
{\it The ISSUP Strategic Review}, December 1983, pp. 2-8, published by 
the Institute for Strategic Studies, University of Pretoria,
Pretoria 0002, South Africa.]
\bigskip\bigskip
\centerline{\bf NUCLEAR TERRORISM AND REALITY}
\bigskip
\centerline{J E St Sauver}
\medskip
\centerline{University of Alaska}
\bigskip\bigskip
\centerline{\bf 1. INTRODUCTION}
\bigskip
NBC's recent television movie {\it Special Bulletin} mistakenly overemphasized
the importance of media coverage to nuclear terrorism while obscuring the true
magnitude of nuclear terrorism's threat to national security. 
Executive Producer Don Ohlmeyer chose to present an unrealistic 
depiction both of how
nuclear terrorism would probably be conducted and of how the United States 
might respond to such an event. In {\it Special Bulletin} the bomb does 
explode, Charleston does get razed, a half-dozen National Emergency Search
Team (NEST) technicians do get killed, but most of the population of
Charleston is successfully evacuated before the bomb explodes and some scant
days later America is portrayed as functioning as if nothing had happened at
all. That is unrealistic and dangerously inaccurate. With or without media
participation, the destruction of a major American city by a terrorist 
nuclear explosive would rock that country and the industrialized world in 
general. There really is no justification for {\it Special Bulletin's}
generally optimistic more-or-less happy ending.
\bigskip\bigskip
\centerline{\bf 2. 
THE MEDIA DOES NOT PLAY AN IMPORTANT ROLE IN NUCLEAR TERRORISM}
\bigskip
Unlike conventional terrorist violence, {\it bona-fide} nuclear 
terrorism{\baselineskip=1.3ex{\footnote{$^{1}$}{{\it Bona-fide}
nuclear terrorism is nuclear terrorism conducted by an individual
or group actually having control over the detonation of one or more 
nuclear devices; distinguish {\it bona-fide} nuclear terrorism from
baseless threats of nuclear attack made by parties without control
of a nuclear device which might otherwise also properly be called ``nuclear
terrorism.''}}} is a self-publicizing phenomenon. Without media 
coverage, conventional terrorist violence has an impact only on 
those it injures, those few bystanders who witness the violence and
those few others who learn of the incident by word of mouth. Unlike the
intimacy of a letter bomb or the inherently personal nature of a gun
shot or even the fraternality of a hand grenade thrown into a crowded
restaurant, the explosion of a nuclear weapon in a major American city is
an inherently public event. It is a public event by the very nature of 
the scope of its impact: a nuclear explosion kills so very many people
and would be witnessed by so very many more that there is no way it could
help but be a public event. Like the coming of the Messiah or the collision
of the earth with another planet, a nuclear explosion in a major city 
needs media coverage the way a blind man needs bifocals. A nuclear explosion
would obtain and hold public interest and attention quite nicely without
the service of a public relations expert or the attention of a television 
news crew.
\bigskip
An insurgent nuclear explosion tacitly conveys a highly emotional message:
no matter who you are, no matter where you live, no matter how much your
country spends on its defense, you too could be annihilated by a terrorist
nuclear weapon and there is virtually nothing you can do to avoid that.
Nuclear weapons are non-denominational and non-discriminatory, unswayed by
implorations and unmoved by imprecations. You cannot bargain with a nuclear
bomb, you cannot intimidate a nuclear bomb and you cannot coerce a nuclear
bomb. You can only attempt to manipulate the person(s) behind the nuclear
explosive, and if that person or persons cannot be identified, there is 
nothing which can be done.
\bigskip
A nuclear explosion initiated by an unknown entity is in many ways the 
ultimate shadowy and threatening symbol. Apocalyptic and catastrophically
destructive with the underlying threat of a lingering death for those who
do survive the initial horror, a nuclear explosion is the ideal instrument
of mass terror. In many ways, the greater the uncertainty and the less media
coverage a nuclear terrorist receives, the greater the terror an
insurgent nuclear explosion would create in the population at large. Yet, if
the terrorists do receive media coverage, the fear engendered by the true
magnitude of the threat posed by nuclear terrorism would probably roughly
equal the fear of the unknown which would dominate without media coverage.
Media coverage of nuclear terrorism can change the type of fear produced by
nuclear terrorism, however it cannot be said to either substantially
aggravate or substantially alleviate nuclear insurgency's general ability to
terrorize the public at large. Media coverage is just not a very important
consideration in cases of {\it bona-fide} nuclear terrorism.
\headline={\hfill\folio\voffset=2\baselineskip}
\count0=2
\bigskip\bigskip
\centerline{\bf 3. THERE WILL BE NO WARNING OF AN IMPENDING NUCLEAR BLAST}
\bigskip
Media coverage of nuclear terrorism is likely to be a very chancy thing
since bona-fide nuclear 
terrorists{\baselineskip=1.3ex{\footnote{$^{2}$}{For an excellent 
discussion of terrorist characteristics, including the
likelihood that terrorists would in fact be rational, see Neil 
C. Livingstone's {\it The War Against Terrorism,} Lexington Books, 
Lexington MA: 1982, pages
31-33.}}} would not inform authorities that a nuclear device is about to be
detonated. Since this assumption runs contrary to the fundamental assumption
of literally every nuclear terrorism scenario, that is, the assumption that 
nuclear terrorists would use the threat of a nuclear detonation to obtain
leverage in negotiations with authorities, it is worth taking a look at why
rational {\it bona-fide} nuclear terrorists would not announce an imminent
nuclear explosion.
\bigskip
Quite simply, rational nuclear insurgents would not announce an imminent 
nuclear explosion because they would gain nothing by doing so. In fact, 
since advance warning of an impending nuclear explosion is of primary
importance in determining the maximum potential effectiveness of any response
to such an event, rational nuclear insurgents would actually strive at 
all costs to avoid even the mere suspicion by authorities that a nuclear 
explosion might occur at some time in the future. If insurgents provide a
warning long before the explosion is to occur, the nuclear explosive may be 
located and safely neutralized{\baselineskip=1.3ex{\footnote{$^{3}$}{Because
of the almost certain presence of anti-tamper mechanisms, it is expected that
any neutralization effort will probably use either shaped charges or high
energy lasers to dismantle the weapon without inducing a nuclear detonation.}}}
before it can be detonated, or actions can be taken to mitigate the effects of
the explosion on the target or targets. The announcement-to-detonation delay
provides a crucial opportunity for mobilization and deployment of response
forces. In {\it Special Bulletin} this delay period was exploited to effect
the evacuation of Charleston and was used to stage the attack on the insurgent
weapon site by the commando team. In reality, authorities would not be given
any such respite, they would not be given the chance to stage such an assault
upon the weapons emplacement location, in fact, they would not even know that
the weapon existed until it had been detonated.
\bigskip
Insurgents who actually control a nuclear capability have a bilateral 
credibility problem. On the one hand, even the mere suspicion that malevolent
actors may be developing a nuclear capability may be sufficient to induce
the mobilization of radiological detection and weapon neutralization teams;
however, on the other hand, it may require an actual demonstration 
detonation for the insurgents to achieve determinative credibility 
sufficient to allow them to dictate state policies. While government officials
may implement precautionary measures against (what they perceive to be) the
rare chance that an insurgent nuclear weapon actually exists, until the
insurgents have shown that they have the capacity to produce high-yield
nuclear explosives and the will to use them against major population 
concentrations, officials certainly will not agree to any substantive
terrorist demands. Having never previously suffered a {\it bona-fide}
incident of nuclear terrorism, but having endured scores of baseless nuclear
hoaxes, authorities will quite naturally be highly skeptical of insurgent
claims that a nuclear explosion will occur unless their demands are met.
Until a non-state improvised nuclear weapon has actually been detonated by some
party, insurgents will find it nearly (if not totally) impossible to use
nuclear weapons as an effective instrument of coercion or extortion. Explosion
of at least one nuclear weapon is, therefore, a condition precedent which must
be satisfied before non-state nuclear weapons (or the threat of non-state
nuclear weapons) can effectively be used to blackmail a major state. 
\bigskip\bigskip
\centerline{\bf 4. 
NUCLEAR WEAPON USE: EXTORTION OR DIRECT POLICY IMPLEMENTATION?}
\bigskip
Alternatively, insurgents can use nuclear weapons as direct instruments of
policy implementation. The difference between using a nuclear explosive to 
blackmail a state and the use of a nuclear weapon to directly implement
policy is analogous to the difference between kidnapping and assassination.
For a kidnapping to be successful, kidnappers require the cooperation of those
who value the one who is kidnapped. If an executive's company or a child's
parents refuse to raise the ransom demanded, the kidnapping is essentially
futile and a failure. Assassination, however, does not require the
cooperation of its victim to be successful. An individual can be killed 
despite the most diligent and adamant efforts to prevent such an event.
Similarly, nuclear terrorists attempting to use an improvised nuclear
explosive device in a 
``classic''{\baselineskip=1.3ex\footnote{$^{4}$}{The classic nuclear
blackmail scenario which has evolved over many years envisions a small team
of terrorists fabricating an improvised nuclear explosive device from stolen
strategic nuclear materials and then using that weapon for leverage in 
negotiations with authorities, threatening to destroy a major city unless 
their demands are met.\par}} nuclear blackmail scenario require cooperation
from the authorities in order to succeed, while nuclear terrorists using
their nuclear explosive as an instrument of direct policy implementation can
succeed without any cooperation or even acknowledgement or recognition by
their target. Insurgent nuclear capabilities then become part of a wider range
of insurgent operational capabilities, complementing and enhancing the normal
range of non-nuclear insurgent unconventional warfare operations when and where
particular circumstances are uniquely well suited to the employment of nuclear
weapons. Rather than threatening to blow up a government unless that 
government agrees to meet terrorist demands, if insurgents were using their
nuclear weapon as a direct instrument of policy implementation, they would
simply remove any governmental entity hindering achievement of their 
objectives. If insurgents want the United States to stop producing nuclear
weapons for example, rather than futilely attempting to pressure the United
States into doing so, insurgents would simply use their own nuclear weapon to
destroy major American nuclear weapon production facilities, thereby directly
achieving their goal.
\bigskip
The use of nuclear weapons as direct policy implementation tools is not ideal,
however. That strategy has the disadvantage that it cannot compel
affirmative acts --- you cannot use a nuclear weapon to make someone do
something. A nuclear weapon cannot induce hypnotic obedience nor
automaton-like unthinking compliance $\ldots$ a nuclear weapon can be used 
in only two ways: in a nuclear blackmail scenario in an attempt to intimidate
an entity into accepting some prescription (however the target of that
coercive effort does not have to necessarily succumb to that suasion), or
it can be used to eliminate opposition or bring about policy implementation
by destruction of that which hinders or impedes attainment of the
insurgent policy objectives. A nuclear weapon can force inaction (through
physical destruction of a person, group, or government body), but it cannot
force action; even under threat of a nuclear detonation, people still have
free will and the capacity to refuse to act.
\bigskip
Of the two options, using nuclear weapons as a means of blackmailing
governments, or using nuclear weapons as a tool with which to directly 
implement policy, the second option is, without a doubt, far more practical.
While the spectre of incineration of a states's cities with nuclear weapons
is an extremely strong coercive stimulus, it can be said with some certainty
that there would no doubt be at least some policy makers with the will to
resist its impetus, just as heads of state do not currently succumb to threats
of assassination. Virtually all customary extortion demands (such as cash
payments, release of prisoners, publication of admissions of culpability, etc.)
can be coerced from authorities with non-nuclear threats. Threatening to blow
up an airliner or threatening to execute hostages in an embassy is more than
sufficient to coerce the desired response in most cases. When threats to blow up
an airplane or threats to kill hostages prove insufficient, it is generally
because a government feels there that there is no way that they can concede to
a particular insurgent demand (or set of demands) under any condition
(including threat of nuclear holocaust) while still remaining a recognizable
semblance of what they once were. That is, if a state can, you don't need a
nuclear weapon to force it to actually do so; if a state can't, introducing a
nuclear weapon won't change anything. This was the situation in {\it Special
Bulletin}: the United States government felt that there was no way that they
could afford to hand over the demanded nuclear weapon ``detonator modules,''
even under threat of a nuclear explosion. There is a certain logic to this; one
can never be sure just what the yield of a particular nuclear weapon will be,
especially the yield of an improvised device. If it is small, and many experts
expect that an improvised nuclear explosive would indeed have only a
comparatively small yield, a state might be better off trying to ride out the
destruction produced by such a device rather than blithely going along with
the certain metamorphosis that would be caused by acceding to what would no 
doubt be extreme insurgent demands. This seems to be current American policy.
\bigskip
However, it is eminently reasonable at this point to assume that insurgents do
have the capability of producing high yield --- even megaton yield --- nuclear
weapons.{\baselineskip=1.3ex\footnote{$^{5}$}{For a discussion of 
the logistics of thermonuclear
insurgency, see `\thinspace ``The H-Bomb 
Secret'' Reconsidered' by this author, to
be published in {\it Strategic Studies,} the quarterly journal of the 
Institute of Strategic Studies, Islamabad, Pakistan. [See Volume VII, Number 4,
Summer 1984, pages 66-92.]\par}} Fully all of the critical nuclear
weapon design
and fabrication information unique and essential to thermonuclear weapon 
conception and production is now publicly available. The supplementary
material requirements necessary to convert a fusion-boosted fission explosive
into a thermonuclear device are, with but few minor exceptions, non-nuclear and
non-explosive, and there is no reason why the vast majority of the secondary
system (the apparatus which converts a low yield fission bomb into a high yield
fusion bomb) cannot be let for commercial production in a contract job shop.
Because the key to thermonuclear weapon production is procurement of sufficient
fissionable material for use in the fusion boosted fission device (which is
the thermonuclear weapon's trigger) and for use in the thermonuclear weapon's
``spark plug,'' if one accepts the fact that insurgent production of any
nuclear weapon is a possibility, one is also forced to consider the distinct
possibility that insurgents may elect to expend the extra effort and attempt
to produce a thermonuclear weapon rather than a ``mere'' fission weapon.
\bigskip
Of course, the probable ability of insurgents to produce megaton-range nuclear
weapons is quite significant. An underlying tacit assumption of the current
U.S. nuclear-terrorism response policy is that if the worst comes to the worst,
society can soak up the worst that terrorists can dish out and still survive in
a culture reasonably approximating that which existed before the insurgent
nuclear explosion. That is, in many ways, the underlying theme of the end of
{\it Special Bulletin}. The crucial assumption is that society can soak up the
worst that terrorists can hit us with; that we can lose Charleston at the 
beginning of the week and be almost unaware of that catastrophe at the end of
the week.
\bigskip
If insurgents are only able to produce kiloton-range weapons, the 
assumption that society may be able to soak up that non-state nuclear 
destruction may be a sound one. But, if insurgents can command thermonuclear
firepower, the targeted state may very well not be able to withstand the
assault upon it. Detonation of a ten-megaton weapon, for example, would
produce, as a minimum, a significant hazard zone of some two thousand
six hundred and forty square miles --- a circular region with a radius of
about twenty-nine miles from ground 
zero.{\baselineskip=1.3ex\footnote{$^{6}$}{Estimate based upon
U.S. Army Field Manual FM 3-22, Fallout Prediction.\par}}
Keep in mind that
that figure does not fully encompass the widely variable, but always serious
contaminative effects of fallout from the burst. Of course, in some respects
the discussion of likely terrorist weapon yields is moot since 
significant skepticism still exists about the ability of insurgents to 
produce any kind of improvised nuclear weapon whatsoever, at least in the
minds of some strategically placed government officials. Therefore, my
earlier comment about the impracticality of using nuclear weapons in attempts
to blackmail governments must be reaffirmed; unfortunately states may be
risking more than they realize when they both discount the possible effects
of {\it bona-fide} nuclear terrorism and base their response strategies on
unrealistic nuclear ``blackmail'' scenarios. Nuclear blackmail is not
practical, but using improvised nuclear weapons to directly implement policy
is. The authorities would not even have the chance to respond before the
weapon is detonated, and afterwards the devastation would be incomprehensibly
vast. Detonation of an improvised nuclear weapon in any American city, with
or without media coverage, is not something which would be forgotten within a
week of its occurrence. We would be a nation reduced to repeating Marlon
Brando's line from {\it Apocalypse Now}, ``The horror, the horror.''
\vfill\eject\end
